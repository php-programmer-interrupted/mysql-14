-- Сделать выборку полной информации о всех футболках в магазине
USE shop;

-- Промежуточный вспомогательный запрос
SELECT * FROM product
	INNER JOIN category ON product.category_id = category_id;

-- Решение ДЗ
SELECT product.id, brand.name, product_type.name, category.name, product.price
	FROM product
		INNER JOIN category ON product.category_id = category.id
		INNER JOIN brand ON brand.id = product.brand_id
		INNER JOIN product_type ON product_type.id = product.product_type_id
			WHERE product_type_id = 10 ORDER BY product.id;


SELECT product.id, brand.name as "Бренд", product_type.name as "Тип продукта", 
category.name as "Категория", product.price as "Цена"
	FROM product
		INNER JOIN category ON product.category_id = category.id
		INNER JOIN brand ON brand.id = product.brand_id
		INNER JOIN product_type ON product_type.id = product.product_type_id
			WHERE product_type_id = 10 ORDER BY product.id;